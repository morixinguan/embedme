LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := utelnetd

LOCAL_CFLAGS := -pipe -DSHELLPATH=\"/bin/login\" -Wall
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_INC_PATHS := \
	$(LOCAL_PATH)

LOCAL_SRC_FILES := \
	utelnetd.c

include $(BUILD_EXECUTABLE)