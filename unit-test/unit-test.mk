LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := unit-test

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
#LOCAL_LDFLAGS := -lsdbus++ -llzma -lmount -lblkid -luuid -lcap -lsystemd
LOCAL_LDFLAGS += -lembx -lemb -lsocketcan -lev -lcurl -lconfig++ -lpthread -lrt -ldl

LOCAL_LIB_PATHS :=

LOCAL_INC_PATHS := \
	$(PROJECT_ROOT)/libemb \
	$(PROJECT_ROOT)/libembx \
	$(LOCAL_PATH)

LOCAL_SRC_FILES := \
	unit-test.cpp

include $(BUILD_EXECUTABLE)
