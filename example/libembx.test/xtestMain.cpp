/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include "Tracer.h"
#include "ArgUtil.h"

using namespace libemb;

extern void testConfig(void);
extern void testEVIO(void);
extern void testJSON(void);

static void menu_print()
{
	TRACE_YELLOW("==================================================\n");
	TRACE_YELLOW("libembx Module Test Build on %s %s\n",__DATE__,__TIME__);
	TRACE_YELLOW("==================================================\n");
    TRACE_YELLOW("01--> Config Test.\n");
	TRACE_YELLOW("02--> EVIO Test.\n");
	TRACE_YELLOW("03--> JSONData Test.\n");
    TRACE_YELLOW(" q--> quit test program.\n");
    TRACE_YELLOW("==================================================\n");
}
/* 测试主程序 */
int main(int argc,char* argv[])
{    
	std::string value;
    Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);
	ArgOption argOption;
	argOption.addOption("h", 0);
	argOption.parseArgs(argc,argv);
	if (argOption.getValue("h",value))
	{
		TRACE_YELLOW("help info\n");
		FilePath filePath(argv[0]);
		TRACE_YELLOW("%s [-h] \n",CSTR(filePath.baseName()));
		return RC_OK;	
	}
	InputGet input;
	while(1)
    {
    	menu_print();	
		input.waitInput();
		if(input.match("quit") || input.match("q"))return 0;
		else if(input.match("01"))  testConfig();
		else if(input.match("02"))  testEVIO();
		else if(input.match("03"))  testJSON();
        else
        {
        	TRACE_RED("Unknown command: \"%s\"\n",input.toCString());
        }
	}
	return 0;
}

