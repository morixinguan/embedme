LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SUB_DIR:=opencore-amr-0.1.5
SUB_CONFIGURE_SCRIPT:=./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST)

include $(BUILD_SUBCONF)