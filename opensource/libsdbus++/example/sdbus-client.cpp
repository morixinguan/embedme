#include "SDBus.h"
#include <stdio.h>
using namespace libemb;

int main()
{
	SDBusConnection conn;
	if(conn.openUser()!=RC_OK)
	{
		return -1;
	}
	int sum=0;
	SDBusObjectDummy& client = SDBusObjectDummy::getInstance();
   	client.callMethod(conn,true,"com.cloudminds.myobject", "/com/cloudminds/Calculator", "com.cloudminds.Calculator", "add", "ii", 100,200);
	client.getMethodReply("i", &sum);
	TRACE_INFO("send add msg: 100+200=%d\n",sum);
    return 0;
}

