#include "SDBus.h"
#include <stdio.h>
using namespace libemb;
class Calculator:public SDBusObject<Calculator>{
	DECL_SDBUSOBJECT(Calculator)
	{
		//do init
	}
public:
    virtual ~Calculator(){};
	static int handleAddMsg(SDBusMessage* msg, void *userdata, SDBusError* err)//此函数作为methodCall回调函数,必须为static
    {
    	int a,b;
		//TRACE_INFO("---handleAddMsg---!\n");
		Calculator& calc = Calculator::getInstance();
    	int rc = calc.getMethodArgs(msg, "ii", &a,&b);
		rc = calc.replyMethodReturn(msg, "i", a+b);
		return rc;
	}	
};

int main()
{
	SDBusConnection conn;
	if(conn.openUser(true)!=RC_OK)
	{
		return -1;
	}
	/* 创建服务端Object */
    Calculator& calc = Calculator::getInstance();
	calc.setDBusObject("/com/cloudminds/Calculator", "com.cloudminds.Calculator");

	/* 注册消息回调 */
	SDBusVTable vtable[]={
	SDBusVTableRowStart(0),
	SDBusVTableRowMethod("add", "ii", "i", &Calculator::handleAddMsg),
	SDBusVTableRowEnd
	};
	calc.setVTable(conn,vtable,3);

	/* 循环接收消息 */
	conn.requestName("com.cloudminds.myobject");
	while(1)
	{
		conn.process(1000000);
	}
    return 0;
}

