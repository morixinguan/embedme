#include "FSMachine.h"

namespace libemb{
FSHandler::FSHandler(std::shared_ptr<FSMachine> stateMachine)
{
    m_stateMachine = stateMachine;
}
FSHandler::~FSHandler()
{
}

FSMachine& FSHandler::getStateMachine()
{
    return *m_stateMachine;
}

FSMachine::FSMachine()
{
}
FSMachine::~FSMachine()
{

}
bool FSMachine::registerState(int stateID,std::shared_ptr<FSHandler> stateHandler)
{
    auto iter = m_stateHandlerMap.find(stateID); 
	if (iter!=m_stateHandlerMap.end())
	{
		return false;
	}
    m_stateHandlerMap.insert(std::make_pair(stateID, stateHandler));
    return true;
}

void FSMachine::setState(int stateID)
{
    auto iter = m_stateHandlerMap.find(stateID); 
	if (iter==m_stateHandlerMap.end())
	{
		return;
	}
    iter->second->handleState();
    m_currState = stateID;
}

int FSMachine::getState()
{
    return m_currState;
}
}
