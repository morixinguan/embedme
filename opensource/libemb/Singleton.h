/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __SINGLETON__H__
#define __SINGLETON__H__

/* 使用模板类创建单例类
 * 例程:
 * class Dummy: public Singleton<Dummy>{
 * DECL_SINGLETON(Dummy){};
 * public:
 *     virtual ~Dummy();
 *     ...
 * };
 */
namespace libemb{

#define DECL_SINGLETON(ClassName)	 private:friend class libemb::Singleton<ClassName>;ClassName()
template<typename T>
class Singleton{
public:
	static T& getInstance()
	{
		static T instance;
		return instance;
	}
	virtual ~Singleton(){};
protected:
	Singleton(){};
	Singleton(const Singleton& ){};
	Singleton& operator=(const Singleton&){};	
};

}
#endif
