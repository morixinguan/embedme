/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __STRING_UTIL_H__
#define __STRING_UTIL_H__

#include "BaseType.h"
#include <iostream>
#include <vector>

namespace libemb{
using namespace std;
/**
 *  \file   StrUtil.h
 *  \class  StrUtil
 *  \brief  字符串工具类
 */
class StrUtil{
public:
    static string toLower(const char* str);
    static string toUpper(const char* str);
    static string stringFormat(const char * fmt, ...);
    static string replaceString(const string& source,const string str,const string rep);
    static string findString(const string& source, const string& start, const string& end);
    static string findString(const string& source, const string& pattern, const string& before, const string& after);
    static string trimHeadWith(const string& source,const string& trimch);
    static string trimTailBlank(const string& source,bool trimHead=true, bool trimTail=true);
    static string trimAllBlank(const string& source);
    static string trimHeadUnvisible(const string& source);
    static vector<string> splitString(const string& source,const string& splitFlag,bool withFlag=false);
    static vector<string> cutString(const string& source,const string& startFlag,const string& stopFlag,const string& cutFlag);
    static int patternCount(const string& source, const string& pattern);
    static int charToInt(char ch);/* 字符转换为整数 */
    static char intToChar(int val);/* 整数转换为字符 */
    static int hexBinarize(const string hexStr,char* buffer,int len);/* 16进制可读字符串转换为二进制字节数据 */
    static string hexVisualize(const char* buffer,int len);/* 二进制字节数据转换为16进制可读字符串 */

    static int hexStrToInt(const char* hexStr);
    static int stringToInt(std::string intStr);
	static string intToString(int num);
    static bool isHexString(const char* hexString);
    static bool isIntString(const char* intString);
	
    /* C风格的解析函数(一般用来解析固定分隔符的字符串,如AT指令) */
    char* parseNextSection(const char* source,uint32* startPos,const char* endflag,uint32* section_len);
    bool parseNextString(const char* source,uint32* startPos, const char* endflag, char* strbuf,uint32 buflen);
    bool parseNextInt(const char* source,uint32* startPos,const char* endflag,sint32* result);
};
}
#endif