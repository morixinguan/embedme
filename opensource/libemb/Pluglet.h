/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __PLUGIN_UTIL_H__
#define __PLUGIN_UTIL_H__

#include "BaseType.h"
#include <map>

namespace libemb{
enum class PlugletType
{
	Lazy,	/* 懒惰式加载符号 */
	Now 	/* 立即加载所有符号 */
};

class Pluglet{
public:
	Pluglet();
	~Pluglet();
	bool isOpen();
	bool open(std::string plugPath,PlugletType type);
	bool putSymbol(std::string symName);
	void* getSymbol(std::string symName);
	void close();
private:
	void* m_plugHandle{NULL};	
	std::map<std::string,void*> m_symbolMap;
};
}
#endif

