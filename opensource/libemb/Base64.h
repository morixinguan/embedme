/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __BASE64_H__
#define __BASE64_H__

#include "BaseType.h"

namespace libemb{
class Base64{
public:
	bool b64Encode(const std::string& binDataIn,std::string& b64DataOut);
	bool b64Decode(const std::string& b64DataIn,std::string& binDataOut);
	bool b64EncodeUrlSafe(const std::string& binDataIn,std::string& b64DataOut);
	bool b64DecodeUrlSafe(const std::string& b64DataIn,std::string& binDataOut);
private:
	char findIndex(const char *str, char c);
};

} 
#endif

