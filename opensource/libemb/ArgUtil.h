/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __ARG_INPUT_H__
#define __ARG_INPUT_H__

#include "BaseType.h"
#include <getopt.h>
#include <map>
#include <vector>

namespace libemb{
#define $LONG_OPT_MAX	20		/* 最大支持的长参数个数 */
class ArgOption{
public:
	ArgOption();
	~ArgOption();
	bool addOption(std::string option,int args=-1);/* args=0:不带参数;args=1:带一个参数;其他值:可带可不带 */
	void parseArgs(int argc, char* argv[]);				/* 解析参数     */
	bool getValue(std::string option,std::string& value);	/* 获取参数值 */
private:
	std::map<std::string,std::string> m_optionMap;
	std::string m_shortOptions{""};
	std::string m_shortOptionPattern{""};
	std::vector<std::string> m_longOptions;
	struct option m_longOptionsPattern[$LONG_OPT_MAX];
};

class InputGet{
public:
	InputGet();
	virtual ~InputGet();
	void waitInput();
	bool match(std::string item);
	int size();
	int toInt();
	float toFloat();
	std::string toString();
	const char* toCString();	
private:
	std::string m_inputStr{""};
};
}
#endif
