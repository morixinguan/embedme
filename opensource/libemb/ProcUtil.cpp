/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014-2020 @ ShenZhen ,China
*******************************************************************************/
#include "Tracer.h"
#include "ProcUtil.h"
#include "StrUtil.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <iostream>

#define  $TIMEOUT_EXECUTE    (3)     /* 默认超时时间为2秒 */

using namespace std;
namespace libemb{
/**
 *  \brief  执行命令并取得命令输出字符串.
 *  \param  cmd 命令字符串.
 *  \param  result 命令输出.
 *  \param  timeoutSec 命令超时时间(seconds,最小为3秒).
 *  \return 命令执行成功返回STATUS_OK,命令执行失败返回STATUS_ERROR.
 */
int ProcUtil::execute(std::string cmd, std::string& result, int timeoutSec)
{
	FILE* fp = popen(CSTR(cmd),"r");
	if (NULL==fp)
	{
		TRACE_ERR("ProcUtil::execute(%s), popen failed.\n",CSTR(cmd));
		return RC_ERROR;
	}
	std::string procName = StrUtil::trimHeadUnvisible(cmd);
    result="";
    if (timeoutSec<0) 
    {
    	procName = "";
    }
	timeoutSec = MAX(timeoutSec,$TIMEOUT_EXECUTE);
    int tout = timeoutSec;
    time_t startTime = ::time(NULL);
    while (1)
    {
        time_t endTime = ::time(NULL);
        if ((endTime-startTime)<0 || 
            (endTime-startTime)>=timeoutSec) 
        {
            break;
        }
        if (tout <= 0)
        {
            break;
        }
        int fd = fileno(fp);
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);
        struct timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec= 0;
        int rc = select(fd + 1, &readfds, NULL, NULL, &tv);
        if (rc<=0 || (!FD_ISSET(fd, &readfds)))
        {
            tout -= 1;
            continue;
        }
        else
        {
            char buf[256]={0};
            rc = fread(buf, 1, sizeof(buf), fp);
            if(rc<=0)
            {
                break;
            }
            result += std::string(buf,rc);
        }  
    }
	if (!procName.empty())
	{
		killProc(procName);/* 杀掉进程 */
	}
	pclose(fp);
	return RC_OK;
}

/**
 *  \brief  执行命令并取得命令输出字符串.
 *  \param  cmd 命令字符串.
 *  \param  timeoutSec 命令超时时间(seconds,最小为3秒).
 *  \return 成功返回命令输出,失败返回空字符串.
 */
std::string ProcUtil::execute(std::string cmd, int timeoutSec)
{	
    std::string result="";
	if (RC_OK==execute(cmd, result ,timeoutSec))
	{
		return result;
	}
	return result;
}

int ProcUtil::createProc(std::string procName, std::string args,std::string envs,int delays)
{
	char* pargs[65]={0};
	char* penvs[65]={0};
	std::vector<std::string> vecargs = StrUtil::splitString(args, " ",false);
	std::vector<std::string> vecenvs = StrUtil::splitString(envs, " ",false);
	if (procName.empty() || vecargs.size()>64 || vecenvs.size()>64)
	{
		TRACE_ERR("param error, proc:%s, args:%s, envs:%s!\n",CSTR(procName),CSTR(args),CSTR(envs));
		return RC_ERROR;
	}
	for(auto i=0; i<vecargs.size(); i++)
	{
		pargs[i] = (char*)CSTR(vecargs[i]);
	}

	for(auto i=0; i<vecenvs.size(); i++)
	{
		penvs[i] = (char*)CSTR(vecargs[i]);
	}
	
	int pid = fork();
	if (pid<0)
	{
		TRACE_ERR("ProcUtil::createProc, fork error: %s!\n",ERRSTR);
		return RC_ERROR;
	}
	else if(pid==0)
	{
		if (delays>0)
		{
			sleep(delays);
		}
		execvpe(CSTR(procName),pargs,penvs);
		TRACE_ERR("exec error: %s %s\n",CSTR(procName),CSTR(args));
		exit(errno);
	}
	return pid;
}

void ProcUtil::killProc(std::string procName)
{
	std::vector<int> pids = getPidsByName(CSTR(procName));
	for(auto i=0; i<pids.size(); i++)
	{
		kill(pids[i],SIGKILL);
	}
}
std::vector<int> ProcUtil::getPidsByName(const char* processName)
{
    std::vector<int> pidList;
    Directory dir;
    dir.enter("/proc");
    std::vector<std::string> fileNameList = dir.listAll();
    int num = fileNameList.size();
    for(auto i=0; i<num; i++) 
    {
        if (!StrUtil::isIntString(CSTR(fileNameList[i])))
        {
            continue;
        }
        File file;
        std::string statusFile = "/proc/"+fileNameList[i]+"/status";
        if(!file.open(CSTR(statusFile), IO_MODE_RD_ONLY))
        {
            //TRACE_ERR("ProcUtil::getPidsByName,can't open file:%s.\n",CSTR(statusFile));
            continue;
        }
        std::string firstLine;
        if(RC_ERROR==file.readLine(firstLine))
        {
            //TRACE_ERR("ProcUtil::getPidsByName,read file error:%s.\n",CSTR(statusFile));
            continue;
        }
        std::vector<std::string> result=StrUtil::splitString(firstLine,":");
        if (result.size()!=2) 
        {
            continue;
        }
        std::string pid = StrUtil::trimTailBlank(result[1]);
        if (pid==processName) 
        {
            pidList.push_back(atoi(CSTR(fileNameList[i])));
        }
    }
    return pidList;
}
ProcFifo::ProcFifo()
{
}

ProcFifo::~ProcFifo()
{
}

bool ProcFifo::open(const char* devName,int ioMode)
{
    if (devName==NULL)
    {
        TRACE_ERR_CLASS("fifo name is NULL.\n");
        return false;
    }
    if(!File::exists(devName))
    {
        int rc=mkfifo(devName,S_IFIFO|0666);
        if (rc<0) 
        {
            TRACE_ERR_CLASS("mkfifo error:%s.\n", ERRSTR); 
            return false;
        }
    }

	if (m_fd >= 0)
	{
		TRACE_ERR_CLASS("Device is already opened!\n");
		return false;
	}

	switch(ioMode)
	{
		case IO_MODE_RD_ONLY:
			m_fd = ::open(devName, O_RDONLY | O_NONBLOCK);
			break;
		case IO_MODE_WR_ONLY:
			m_fd = ::open(devName, O_WRONLY | O_NONBLOCK);
			break;
		case IO_MODE_RDWR_ONLY:
			m_fd = ::open(devName, O_RDWR | O_NONBLOCK);
			break;
		default:
			TRACE_ERR_CLASS("Unsupport IO Mode: %d\n",ioMode);
			return false;
	}

	if (m_fd<0)
	{
		TRACE_ERR_CLASS("Open %s error: %s\n",devName,ERRSTR);
		return false;
	}
	m_devName = devName;
	m_openMode = ioMode;
	return true;
}
}