/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __HTTP_SERVER_H__
#define __HTTP_SERVER_H__

#include "BaseType.h"
#include "Thread.h"
#include "Socket.h"

namespace libembx{
using namespace libemb;
/**
 *  \file   HttpServer.h   
 *  \class  HttpServer
 *  \brief  Http服务器(一个简单的http服务器,支持cgi和静态页面)	
 */
class HttpServer:public TcpServer{
public:
    HttpServer();
    virtual ~HttpServer();
    void setRootPath(const std::string path);
private:
    void onBadRequest(TcpSocket* connSocket);
    void onNotFound(TcpSocket* connSocket);
    void onInternalServerError(TcpSocket* connSocket);
    void onUnimplemented(TcpSocket* connSocket);
    int  readLine(TcpSocket* connSocket, char *buf, int size);
    void sendBack(TcpSocket* connSocket, const char *filename);
    void executeCgi(TcpSocket* connSocket, const char *path,const char *method, const char *query_string);
    void acceptRequest(TcpSocket* connSocket);
    void onNewConnection(Socket *connSocket);
    void taskHandleConnection(void* args);
private:
    std::string m_rootPath;
};
}

#endif
