#ifndef __GPIO_INPUT_H__
#define __GPIO_INPUT_H__

#include "BaseType.h"
#include "Gpio.h"
#include "Thread.h"
#include "ThreadUtil.h"
#include <list>

namespace libemb{
typedef enum{
    GPIO_STATUS_UP=0,  /* 按键抬起 */
    GPIO_STATUS_DOWN,  /* 按键按下 */
}GPIO_STATUS_E;

/**
 *  \file   GpioInput.h   
 *  \class  GpioListener
 *  \brief  GPIO状态监听者.
 */
class GpioListener{
public:
    GpioListener(){};
    virtual ~GpioListener(){};
    virtual void handleGpio(std::string name,int status)=0;
};

/**
 *  \file   GpioInput.h   
 *  \class  GpioInput
 *  \brief  GPIO输入者.
 */
class GpioInput : public Runnable {
public:
    static GpioInput* getInstance()
    {
        static GpioInput instance;
        return &instance;
    }
    ~GpioInput();
    bool addGpio(std::string gpioName,int enValue,int msDelay=1000);
    bool delGpio(std::string gpioName);
    void registerGpioListener(GpioListener* gpioListener);
    void unregisterGpioListener(GpioListener* gpioListener);
private:
    GpioInput();
    void run();
private:
    class InputGpio{
    public:
        std::string m_name; /* gpio名称 */
        int m_enValue;      /* 使能时的值 */
        int m_msDelay;      /* 有效使能时长 */
        int m_msTime;       /* 使能计时 */
        int m_status;       /* 当前状态 */
        GpioImp* m_gpio;    /* gpio对象 */
    };
    std::list<InputGpio*> m_gpioList;
    Thread m_mainThread;
    std::list<GpioListener*> m_listenerList;
    MutexLock m_listenerLock;
    MutexLock m_gpioLock;
};
}
#endif
