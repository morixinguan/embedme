/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MD5_CHECK_H__
#define __MD5_CHECK_H__

#include "BaseType.h"

namespace libembx{
/**
 *  \file   MD5Check.h   
 *  \class  MD5_Context_S
 *  \brief  none
 */
struct MD5Context
{
    libemb::uint32  A,B,C,D;	  /* chaining variables */
    libemb::uint32  nblocks;
    libemb::uint8   buf[64];
    libemb::sint32  count;
};

/**
 *  \file   MD5Check.h   
 *  \class  MD5Check
 *  \brief  MD5校验类
 */
class MD5Check{
public:
    MD5Check();
    ~MD5Check();
    int checkFile(std::string fileName,std::string& md5sum);
    int checkString(std::string srcString,std::string& md5sum);
private:
    void initContext(MD5Context *ctx);
    void transform( MD5Context* ctx, libemb::uint8* data);
    void md5Write(MD5Context *ctx, libemb::uint8* inbuf, int inlen);
    void md5Final(MD5Context *ctx);
};
}
#endif