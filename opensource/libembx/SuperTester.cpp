/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include "SuperTester.h"
#include "Tracer.h"
#include "ThreadUtil.h"
#include "DateTime.h"

using namespace libemb;
namespace libembx{
SuperTester::SuperTester()
{
	m_cycles = 0;
	m_interval =1000;
}
SuperTester::~SuperTester()
{
}

bool SuperTester::initWithSetting(libembx::Settings& setting)
{
	if (setting.exists("cycles"))
	{
		m_cycles = setting["cycles"].toInt();
		m_cycles = MAX(1,m_cycles);
	}
	if (setting.exists("interval"))
	{
		m_interval = setting["interval"].toInt();
		m_interval = MAX(0,m_interval);
	}
	return true;
}
void SuperTester::run()
{
	for(auto i=0; i<m_cycles; i++)
	{
		if (i!=0)
		{
			Thread::msleep(m_interval);
		}
		Time startTime = Time::currentTimeMonotonic();
		int result = testCase();
		Time duration = Time::currentTimeMonotonic()-startTime;
		TRACE_REL_CLASS(">>> testCase %s in cycle:%d/%d, used time: %s\n",(result==RC_OK)?"success":"failed",i+1,m_cycles,CSTR(duration.toString()));
	}
}

}
