#ifndef __MSGHANDLER_H__
#define __MSGHANDLER_H__

#include "BaseType.h"
namespace libemb{
class MsgReporter{
public:
    MsgReporter(){};
    virtual ~MsgReporter(){};
    virtual bool reportMsg(const std::string& msg)=0;
};
}

#endif
