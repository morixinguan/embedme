/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __LOCAL_MSGQUEUE_H__
#define __LOCAL_MSGQUEUE_H__

#include "BaseType.h"
#include "ThreadUtil.h"

#include <map>
#include <vector>

#define MSG_SIZE_MAX    128             /**< 最大消息长度 */
namespace libemb{
/**
 *  \class  QueueMsg_S
 *  \brief  msgbuf消息结构体
 */
typedef struct{
    int m_msgType;                  /**< 消息类型 */
    unsigned char m_dataLen;        /**< 数据长度 */
    char m_data[MSG_SIZE_MAX-1];    /**< 消息数据 */
}QueueMsg_S;

/**
 *  \file   LocalMsgQueue.h   
 *  \class  LocalMsgQueue
 *  \brief  本地消息队列类
 *  \note   仅可用在本地进程中,不可跨进程使用
 */

class LocalMsgQueue{
public:
    ~LocalMsgQueue();
    int sendMsg(QueueMsg_S& msg);
    int recvMsg(QueueMsg_S& msg, int msgType=0);
    int clearMsg(int msgType);
private:
    LocalMsgQueue();
private:
    MutexLock m_mqLock;
    std::vector<QueueMsg_S> m_msgVector;
    friend class LocalMsgQueueFactory;
};

/**
 *  \file   LocalMsgQueue.h   
 *  \class  LocalMsgQueueFactory
 *  \brief  消息队列工厂类	
 */

class LocalMsgQueueFactory{
public:
    /**
     *  \brief  获取LocalMsgQueueFactory单例
     *  \param  void
     *  \return LocalMsgQueueFactory* 
     *  \note   none
     */
    static LocalMsgQueueFactory* getInstance()
    {
        static LocalMsgQueueFactory instance;
        return &instance;
    }
    ~LocalMsgQueueFactory();
    LocalMsgQueue* getMsgQueue(int key);
private:
    LocalMsgQueueFactory();
private:
    MutexLock m_lock;
    std::map<int,LocalMsgQueue*> m_msgQueueMap;
};
}

#endif

