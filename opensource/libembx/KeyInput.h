/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __KEY_INPUT_H__
#define __KEY_INPUT_H__

#ifdef OS_CYGWIN
#else
#include "BaseType.h"
#include "Thread.h"
#include "ThreadUtil.h"
#include <list>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

namespace libemb{
/**
 *  \file   KeyInput.h   
 *  \class  KeyListener
 *  \brief  按键事件监听者.
 */
class KeyListener{
public:
    KeyListener(){};
    virtual ~KeyListener(){};
    virtual void handleKey(int keycode,int keyvalue)=0;
};
/**
 *  \file   KeyInput.h   
 *  \class  KeyInput
 *  \brief  按键输入者.
 */
class KeyInput:public Runnable{
public:
    static KeyInput* getInstance()
    {
        static KeyInput instance;
        return &instance;
    }
    ~KeyInput();
    bool open(const std::string& devName);
    bool close();
    void registerKeyListener(KeyListener* keyListener);
    void unregisterKeyListener(KeyListener* keyListener);
private:
    KeyInput();
    void run();
private:
    int m_fdInput;
    Thread m_mainThread;
    std::list<KeyListener*> m_listenerList;
    MutexLock m_listenerLock;
};
}
#endif
#endif