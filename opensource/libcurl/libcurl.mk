LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_BUILD_DIR:=curl-7.67.0
LOCAL_BUILD_SCRIPT:=./buildconf;./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST) --disable-ldap  --disable-ldaps --without-zlib --without-ssl --without-libidn;make;make install

include $(BUILD_MBSCRIPT)